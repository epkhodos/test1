<?php
//test
	interface Say{
		public function sayName();
	}


	abstract class Native implements Say	{
		private $name;
		public function __construct($name){
		$this->name = $name;
		}
		public function getName() {
			return $this->name;
		}
	}

class Animal extends Native 
{
	private $color;
	private $breed;
	private $sound;
	private $age;
		
	public function __construct($color, $breed, $sound, $age)	{
		
		$this->color = $color;
		$this->breed = $breed;
		$this->sound = $sound;
		$this->age = $age;	}


		public function sayName()	{
		echo "Мое имя: ".$this->name."";	}

		public function sayColor()	{
		echo "Цвет моей шерсти: ".$this->color."";	}

		public function sayBreed()	{
		echo "Моя порода: ".$this->breed."";  }

		public function saySound()	{
		echo "".$this->sound."";  }

		public function sayAge()	{
		echo "Мой возраст: ".$this->age."";	}

}

class Cat extends Animal {
	
	public function say(){
		echo "Я мяукаю так: ";	}
	
}

class Dog extends Animal {
	
	public function say(){
		echo "Я лаю так: ";
	}
	
}


$cat1 = new Cat("Марсель ","черный", "британик", "мяу мяу", "3"); echo "<br>";
$cat1->sayName(); echo "<br>";
$cat1->sayColor(); echo "<br>";
$cat1->sayBreed(); echo "<br>";
$cat1->say();  
$cat1->saySound(); echo "<br>";
$cat1->sayAge(); echo "<br>";

$dog1 = new Dog("Лорд","черный", "коли", "гав гав", "5"); echo "<br>";
$dog1->sayName(); echo "<br>";
$dog1->sayColor(); echo "<br>";
$dog1->sayBreed(); echo "<br>";
$dog1->say(); 
$dog1->saySound();echo "<br>";
$dog1->sayAge(); echo "<br>";

$cat2 = new Cat("Мар", "рыжий", "британик", "мяу мяу", "9"); echo "<br>";
$cat2->sayName(); echo "<br>";
$cat2->sayColor(); echo "<br>";
$cat2->sayBreed(); echo "<br>";
$cat2->say();  
$cat2->saySound(); echo "<br>";
$cat2->sayAge(); echo "<br>";

$dog2 = new Dog("Букет","белый", "домашний", "гав гав", "6"); echo "<br>";
$dog2->sayName(); echo "<br>";
$dog2->sayColor(); echo "<br>";
$dog2->sayBreed(); echo "<br>";
$dog2->say(); 
$dog2->saySound();echo "<br>";
$dog2->sayAge(); echo "<br>";

$dog3 = new Dog("Робот","белый", "домашний", "гав гав", "6"); echo "<br>";
$dog3->sayName(); echo "<br>";
$dog3->sayColor(); echo "<br>";
$dog3->sayBreed(); echo "<br>";
$dog3->say(); 
$dog3->saySound();echo "<br>";
$dog3->sayAge(); echo "<br>";













?>