<?php

	/**
	* 
	*/
	class Human	{
		private $name;

		public function __construct($name) {
			$this->name = $name;
		}
		
		public function say() {
			echo "Меня зовут ".$this->name. " и ";
		}

	}

	class Man extends Human	{
		
		public function beard() {
			echo "У меня растет борода";
		}

	}

	class Woman extends Human {
		public function bearChildren() {
			echo "Я рожаю";
		}
		
	}

$man = new Man("Jeka");
$man->say();
$man->beard();

echo "<br>";

$woman = new Woman("She");
$woman->say();
$woman->bearChildren();


?>